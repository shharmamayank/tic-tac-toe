import React, { useState } from 'react'
import "./Game.css";

function Square({ value, onSquareClick }) {

    return (

        <button className='square' onClick={onSquareClick}>{value}</button>
    )

}
function Board({ xIsNext, squares, onPlay }) {
    // console.log(squares)
    // const [xIsNext, setXIsNext] = useState(true)
    // const [squares, setSquare] = useState(Array(9).fill(null))
    function handleClick(index) {
        if (squares[index] || calculateWinner(squares)) {
            return;
        }
        const nextSquare = squares.slice()
        if (xIsNext) {
            nextSquare[index] = 'X'
        } else {
            nextSquare[index] = 'O'
        }
        onPlay(nextSquare)
    }
    const winner = calculateWinner(squares)
    let status
    if (winner) {
        status = "Winner: " + winner;
    } else {
        status = "Next player: " + (xIsNext ? "X" : "O");
    }
    return (
        <>
            <div className="status">{status}</div>
            <div className='btn-contianer'>
                <div className="board-row">
                    <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
                    <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
                    <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
                </div>
                <div className="board-row">
                    <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
                    <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
                    <Square value={squares[5]} onSquareClick={() => handleClick(5)} />

                </div>
                <div className="board-row">
                    <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
                    <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
                    <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
                </div>
            </div>
        </>
    )
}


export default function Game() {

    console.log(useState())
    const [history, setHistory] = useState([Array(9).fill(null)])
    const [currentMove, setCurrentMove] = useState(0)
    const xIsNext = currentMove % 2 === 0;
    const currentSquare = history[currentMove]

    function handlePlay(nextSquare) {
        const nextHistory = ([...history.slice(0, currentMove + 1), nextSquare])
        setHistory(nextHistory)
        setCurrentMove(nextHistory.length - 1)

    }
    function jumpTo(nextMove) {
        setCurrentMove(nextMove)



    }


    const moves = history.map((sqaures, move) => {
        let description
        if (move > 0) {
            description = 'Go to move #' + move;
        } else {
            description = "game start"
        }
        return (
            <li key={move}>
                <button className='moves-btn-box' onClick={() => jumpTo(move)}>{description}</button>
            </li>
        )
    })


    return (
        <>
            <Board xIsNext={xIsNext} squares={currentSquare} onPlay={handlePlay} />
            <div className="game-info">
                <ol>{moves}</ol>
            </div>
        </>
    )
}

function calculateWinner(squares) {
    const line = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]

    for (let index = 0; index < line.length; index++) {
        const [a, b, c] = line[index]
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[b]
        }

    }
    return null;

}
